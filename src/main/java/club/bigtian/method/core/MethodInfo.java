package club.bigtian.method.core;


import java.util.Arrays;

public class MethodInfo {
    private String name;

    private String[] target;

    private String methodName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getTarget() {
        return target;
    }

    public void setTarget(String[] target) {
        this.target = target;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    @Override
    public String toString() {
        return "MethodInfo{" +
                "name='" + name + '\'' +
                ", target=" + Arrays.toString(target) +
                ", methodName='" + methodName + '\'' +
                '}';
    }


}

package club.bigtian.method.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author daijunxiong
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.METHOD})
public @interface BigMethod {
    String name();

    String[] target() default {};
}

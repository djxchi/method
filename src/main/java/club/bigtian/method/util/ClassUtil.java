package club.bigtian.method.util;

import club.bigtian.method.anno.BigFunction;
import club.bigtian.method.handler.BigFunctionAbstractProcessor;
import com.sun.source.tree.Tree;
import com.sun.tools.javac.tree.JCTree;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.tools.Diagnostic.Kind;

public class ClassUtil {


    public static JCTree.JCAnnotation getAnnoByType(JCTree.JCClassDecl jcClassDecl, Class clazz) {
        JCTree.JCAnnotation jcAnnotation = getAnnoList(jcClassDecl).stream().filter((el) -> el.getAnnotationType().toString().equals(BigFunction.class.getSimpleName())).findFirst().get();
        return jcAnnotation;
    }

    public static List<JCTree.JCAnnotation> getAnnoList(JCTree.JCClassDecl jcClassDecl) {
        return jcClassDecl.getModifiers().annotations;
    }

    public static JCTree.JCVariableDecl getFiled(JCTree.JCClassDecl jcClassDecl, String filedName) {
        Optional<JCTree.JCVariableDecl> first = jcClassDecl.getMembers().stream()
                .filter(el -> el.getKind().equals(JCTree.Kind.VARIABLE))
                .map(el -> (JCTree.JCVariableDecl) el)
                .filter(el -> el.getName().toString().equals(filedName))
                .findFirst();
        return first.get();
    }
}

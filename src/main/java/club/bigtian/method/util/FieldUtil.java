package club.bigtian.method.util;

import club.bigtian.method.handler.BigFunctionAbstractProcessor;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReUtil;
import com.sun.source.tree.Tree.Kind;
import com.sun.tools.javac.tree.JCTree;

import javax.tools.Diagnostic;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class FieldUtil {


    public static List<JCTree> getFields(JCTree.JCClassDecl jcClassDecl) {
        List<JCTree> list = (List) jcClassDecl.defs.stream().filter((el) -> {
            return el.getKind().equals(Kind.VARIABLE);
        }).collect(Collectors.toList());
        JCTree.JCVariableDecl jcVariableDecl = (JCTree.JCVariableDecl) list.get(0);
        System.out.println(jcVariableDecl.toString());
        return list;
    }

    /**
     * 获取字段类型map中的值泛型
     *
     * @param jcClassDecl jc类decl
     * @param filedName   提起名字
     * @return {@code JCTree.JCVariableDecl}
     */
    public static String[] getFiledType(JCTree.JCClassDecl jcClassDecl, String filedName) {
        Optional<JCTree.JCVariableDecl> first = jcClassDecl.getMembers().stream()
                .filter(el -> el.getKind().equals(JCTree.Kind.VARIABLE))
                .map(el -> (JCTree.JCVariableDecl) el)
                .filter(el -> el.getName().toString().equals(filedName))
                .findFirst();
        JCTree.JCVariableDecl jcVariableDecl = first.get();

        String input = jcVariableDecl.toString().split("=")[0];
        BigFunctionAbstractProcessor.messager.printMessage(Diagnostic.Kind.WARNING, input);
        input = input.substring(input.indexOf(',') + 1, input.lastIndexOf('>'));
        String regex = "<(?<content>.+)>";
        return ObjectUtil.defaultIfNull(ReUtil.get(regex, input, "content"), "").replaceAll(" ", "").split(",");

    }

}
